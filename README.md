# Lesson 6 special homework

## Features

1. Basic authentication
2. Add/Remove purchases
3. Admin panel

## Installation

### create virtual environment

1. Create new python virtual environment `python -m venv venv`
2. Activate venv `. venv/bin/activate`
3. Install python requirements `pip install -r requirements.txt`

### init/migrate db

**Note:** if `manage.py` not working try `chmod +x manage.py`

```bash
./manage.py db init
./manage.py db stamp head
./manage.py db migrate
./manage.py db upgrade
```

### insert super user

python:

```python
from werkzeug.security import generate_password_hash
generate_password_hash('password')
```

sqlite:

```sql
INSERT INTO user (username, password, is_super_user) VALUES('admin', 'password_hash', 1);
```
