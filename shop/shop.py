from flask import Flask
from sqlalchemy import event
from sqlalchemy.engine import Engine

from shop.db import db, migrate
from shop.utils import log, datetime_format
from shop.errors import page_not_found
from shop.events import _set_sqlite_pragma
from shop.config import FlaskConfig
from shop.login_manager import login_manager, load_user, unauthorized_callback


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(FlaskConfig)

    # EVENTS
    event.listen(Engine, "connect", _set_sqlite_pragma)

    # DATABASE
    db.init_app(app)
    migrate.init_app(app, db)

    # LOGIN MANAGER
    login_manager.login_view = "auth.login"
    login_manager.init_app(app)
    login_manager.user_loader(load_user)
    login_manager.unauthorized_handler(unauthorized_callback)

    # ROUTES
    app.after_request(log)

    # FILTERS
    app.add_template_filter(datetime_format, "datetime_format")

    # ERRORS
    app.register_error_handler(404, page_not_found)

    # BLUEPRINTS
    from shop.blueprints import auth, shop, admin

    app.register_blueprint(admin.bp)
    app.register_blueprint(auth.bp)
    app.register_blueprint(shop.bp)

    return app


app = create_app()
