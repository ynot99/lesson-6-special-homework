from flask import (
    Blueprint,
    redirect,
    render_template,
    request,
    url_for,
)
from flask.json import jsonify
from flask_login import current_user
from flask.helpers import make_response
from flask_login.utils import login_required

from shop.shop import db
from shop.utils import group_order_items, remove_image, upload_image
from shop.forms import AddItemForm, UpdateItemForm
from shop.models import OrderID, OrderItem, ShopItem, User


bp = Blueprint("admin", __name__, url_prefix="/admin")


@bp.before_request
def admin_before_request():
    if current_user.is_authenticated:
        if not current_user.is_super_user:
            return redirect(url_for("shop.items"))


@bp.route("/items", methods=["GET", "POST"])
@login_required
def manage_items():
    form = AddItemForm()
    if request.method == "POST" and form.validate_on_submit():
        filename = None
        if form.image.data.filename:
            filename = upload_image(form.image.data)

        new_shop_item = ShopItem(
            name=form.name.data,
            type=form.type.data,
            price=form.price.data,
            shop=form.shop.data,
            image_name=filename,
        )
        db.session.add(new_shop_item)
        db.session.commit()

        return redirect(url_for("admin.manage_items"))

    shop_items = ShopItem.query.all()
    return render_template(
        "admin/manage_items.j2", shop_items=shop_items, form=form
    )


@bp.route("/orders/pending", methods=["GET"])
@login_required
def pending_orders():
    order_items = (
        db.session.query(
            OrderID.id,
            OrderItem.quantity,
            OrderItem.fixed_price,
            ShopItem.name,
            ShopItem.shop,
            ShopItem.type,
            User.username,
            OrderID.order_status,
            OrderID.created_at,
        )
        .join(ShopItem)
        .join(OrderID)
        .join(User)
        .filter(OrderID.order_status == False)
        .all()
    )

    grouped_order_items = group_order_items(order_items, True)

    return render_template(
        "admin/pending_orders.j2", grouped_order_items=grouped_order_items
    )


@bp.route("/orders/confirmed", methods=["GET"])
@login_required
def confirmed_orders():
    order_items = (
        db.session.query(
            OrderID.id,
            OrderItem.quantity,
            OrderItem.fixed_price,
            ShopItem.name,
            ShopItem.shop,
            ShopItem.type,
            User.username,
            OrderID.order_status,
            OrderID.created_at,
        )
        .join(ShopItem)
        .join(OrderID)
        .join(User)
        .filter(OrderID.order_status == True)
        .all()
    )

    grouped_order_items = group_order_items(order_items, True)

    return render_template(
        "admin/confirmed_orders.j2", grouped_order_items=grouped_order_items
    )


@bp.route("/item/<int:item_id>", methods=["GET", "POST"])
@login_required
def manage_item(item_id: int):
    form = UpdateItemForm()
    if request.method == "POST" and form.validate_on_submit():
        existing_shop_item = ShopItem.query.get(item_id)
        existing_shop_item.name = form.name.data
        existing_shop_item.type = form.type.data
        existing_shop_item.shop = form.shop.data
        existing_shop_item.price = form.price.data

        db.session.commit()
        return redirect(url_for("admin.manage_items"))

    shop_item = ShopItem.query.get(item_id)
    return render_template(
        "admin/manage_item.j2", shop_item=shop_item, form=form
    )


@bp.route("/item/<int:item_id>/remove", methods=["POST"])
@login_required
def remove_item(item_id: int):
    shop_item = ShopItem.query.get(item_id)

    if shop_item:
        db.session.delete(shop_item)
        if shop_item.image_name:
            remove_image(shop_item.image_name)

        db.session.commit()
        return make_response(jsonify(success=True), 200)

    db.session.rollback()
    return make_response(jsonify(success=False), 400)


@bp.route("/order/<int:order_id>/confirm", methods=["POST"])
@login_required
def confirm_order(order_id: int):
    order = OrderID.query.get(order_id)
    order.order_status = True
    db.session.commit()
    return make_response(jsonify(success=True), 200)


@bp.route("/order/<int:order_id>/remove", methods=["POST"])
@login_required
def cancel_order(order_id: int):
    is_deleted = OrderID.query.filter_by(id=order_id).delete()
    if is_deleted:
        db.session.commit()
        return make_response(jsonify(success=True), 200)

    db.session.rollback()
    return make_response(jsonify(success=False), 400)
