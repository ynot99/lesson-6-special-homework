from flask import (
    Blueprint,
    redirect,
    render_template,
    request,
    url_for,
    jsonify,
    make_response,
    abort,
)
from flask_login import current_user
from flask_login.utils import login_required

from shop.shop import db
from shop.forms import AddToCartItemForm
from shop.utils import get_cart_items_and_total, group_order_items
from shop.models import OrderID, OrderItem, ShopItem, User, UserCart

bp = Blueprint("shop", __name__)


@bp.route("/", methods=["GET"])
def items():
    shop_items = ShopItem.query.all()
    return render_template(
        "shop/items.j2",
        shop_items=shop_items,
    )


@bp.route("/item/<int:item_id>", methods=["GET", "POST"])
@login_required
def item(item_id: int):
    form = AddToCartItemForm()
    if request.method == "POST" and form.validate_on_submit():

        new_cart_item = UserCart(
            user=current_user.id, shop_item=item_id, quantity=form.quantity.data
        )

        db.session.add(new_cart_item)
        db.session.commit()
        return redirect(url_for("shop.item", item_id=item_id))

    shop_item = ShopItem.query.get(item_id)

    is_in_cart = db.session.query(
        UserCart.query.filter_by(
            user=current_user.id, shop_item=item_id
        ).exists()
    ).first()[0]

    if not shop_item:
        abort(404)

    return render_template(
        "shop/item.j2",
        shop_item=shop_item,
        is_in_cart=is_in_cart,
        form=form,
    )


@bp.route("/cart", methods=["GET"])
@login_required
def cart():
    cart_items, total = get_cart_items_and_total()
    return render_template("shop/cart.j2", cart_items=cart_items, total=total)


@bp.route("/orders", methods=["GET"])
@login_required
def orders():
    order_items = (
        db.session.query(
            OrderID.id,
            OrderItem.quantity,
            OrderItem.fixed_price,
            ShopItem.name,
            ShopItem.shop,
            ShopItem.type,
            OrderID.order_status,
            OrderID.created_at,
        )
        .join(ShopItem)
        .join(OrderID)
        .join(User)
        .filter(OrderID.user == current_user.id)
        .all()
    )

    id_grouped_order_items = group_order_items(order_items)

    return render_template(
        "shop/orders.j2", grouped_order_items=id_grouped_order_items
    )


@bp.route("/checkout", methods=["GET"])
@login_required
def checkout():
    cart_items, total = get_cart_items_and_total()
    return render_template(
        "shop/checkout.j2", cart_items=cart_items, total=total
    )


@bp.route("/order/<int:order_id>/remove", methods=["POST"])
@login_required
def cancel_order(order_id: int):
    is_deleted = OrderID.query.filter_by(
        id=order_id, user=current_user.id
    ).delete()
    if is_deleted:
        db.session.commit()
        return make_response(jsonify(success=True), 200)

    db.session.rollback()
    return make_response(jsonify(success=False), 400)


@bp.route("/cart/<int:item_id>/remove", methods=["POST"])
@login_required
def cart_remove_item(item_id: int):
    is_deleted = UserCart.query.filter_by(
        id=item_id, user=current_user.id
    ).delete()
    if is_deleted:
        db.session.commit()
        return make_response(jsonify(success=True), 200)

    db.session.rollback()
    return make_response(jsonify(success=False), 400)


@bp.route("/cart/<int:item_id>/update", methods=["POST"])
@login_required
def cart_update_item(item_id: int):
    new_quantity: int = request.json["quantity"]
    if type(new_quantity) is not int or new_quantity < 1:
        return make_response("Non-valid data was received", 400)

    UserCart.query.filter_by(
        id=item_id, user=current_user.id
    ).first().quantity = new_quantity

    db.session.commit()

    return make_response(jsonify(success=True), 200)


@bp.route("/checkout/confirm", methods=["POST"])
@login_required
def checkout_confirm():
    user_cart = (
        db.session.query(
            UserCart.shop_item,
            UserCart.quantity,
            ShopItem.price,
        )
        .join(UserCart)
        .filter(UserCart.user == current_user.id)
    )

    order_items = []
    new_order = OrderID(user=current_user.id)
    db.session.add(new_order)
    for cart_item in user_cart:
        order_items.append(
            OrderItem(
                orderID=new_order.id,
                shop_item=cart_item.shop_item,
                fixed_price=cart_item.price,
                quantity=cart_item.quantity,
            )
        )

    UserCart.query.filter_by(user=current_user.id).delete()
    db.session.add_all(order_items)
    db.session.commit()

    return make_response(jsonify(success=True), 200)
