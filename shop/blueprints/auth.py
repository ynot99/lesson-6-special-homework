from flask import (
    Blueprint,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import login_user
from flask_login.utils import login_required, logout_user

from shop.forms import RegisterForm, LoginForm
from shop.shop import db
from shop.models import User


bp = Blueprint("auth", __name__)


@bp.route("/register", methods=["GET", "POST"])
def register():
    form = RegisterForm()
    if request.method == "POST" and form.validate_on_submit():
        username = form.username
        password = form.password.data

        new_user = User(username=username.data)
        new_user.set_password(password)

        db.session.add(new_user)
        db.session.commit()

        return redirect(url_for("auth.login"))

    return render_template("auth/register.j2", form=form)


@bp.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if request.method == "POST" and form.validate_on_submit():
        login_user(form.validated_user, remember=form.remember_me.data)
        return redirect(url_for("shop.items"))

    return render_template("auth/login.j2", form=form)


@bp.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("shop.items"))
