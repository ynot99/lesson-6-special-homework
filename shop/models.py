import sqlalchemy
from flask_login import UserMixin
from werkzeug.security import check_password_hash, generate_password_hash

from shop.shop import db


class User(db.Model, UserMixin):
    id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True,
        autoincrement=True,
        nullable=False,
    )
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.Text, nullable=False)
    is_super_user = db.Column(db.Boolean, default=False, nullable=False)
    created_at = db.Column(
        db.DateTime, server_default=sqlalchemy.sql.func.now(), nullable=False
    )

    def get_id(self) -> int:
        return int(self.id)

    def set_password(self, password) -> bool:
        self.password = generate_password_hash(password)

    def check_password(self, password) -> bool:
        return check_password_hash(self.password, password)


class ShopItem(db.Model):
    id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True,
        autoincrement=True,
        nullable=False,
    )
    type = db.Column(db.String(30), nullable=False)
    name = db.Column(db.String(30), nullable=False)
    shop = db.Column(db.String(30), nullable=False)
    price = db.Column(db.Integer, nullable=False)
    image_name = db.Column(db.String, default=None, nullable=True)
    created_at = db.Column(
        db.DateTime, server_default=sqlalchemy.sql.func.now(), nullable=False
    )


class OrderID(db.Model):
    id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True,
        autoincrement=True,
        nullable=False,
    )
    user = db.Column(
        db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"), nullable=False
    )
    order_status = db.Column(db.Boolean, default=False, nullable=False)
    created_at = db.Column(
        db.DateTime, server_default=sqlalchemy.sql.func.now(), nullable=False
    )


class OrderItem(db.Model):
    id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True,
        autoincrement=True,
        nullable=False,
    )
    orderID = db.Column(
        db.Integer,
        db.ForeignKey("orderID.id", ondelete="CASCADE"),
        nullable=False,
    )
    shop_item = db.Column(
        db.Integer,
        db.ForeignKey("shop_item.id", ondelete="CASCADE"),
        nullable=False,
    )
    fixed_price = db.Column(db.Integer, nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    created_at = db.Column(
        db.DateTime, server_default=sqlalchemy.sql.func.now(), nullable=False
    )

    __table_args__ = (
        db.UniqueConstraint(
            "orderID", "shop_item", name="unique_item_per_order"
        ),
    )


class UserCart(db.Model):
    id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True,
        autoincrement=True,
        nullable=False,
    )
    user = db.Column(
        db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"), nullable=False
    )
    shop_item = db.Column(
        db.Integer,
        db.ForeignKey("shop_item.id", ondelete="CASCADE"),
        nullable=False,
    )
    quantity = db.Column(db.Integer, nullable=False)
    created_at = db.Column(
        db.DateTime, server_default=sqlalchemy.sql.func.now(), nullable=False
    )

    __table_args__ = (
        db.UniqueConstraint("user", "shop_item", name="unique_item_per_user"),
    )
