class FlaskConfig:
    SECRET_KEY = "dev"
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "sqlite:///../db.sqlite3"
    IMAGE_UPLOAD_PATH = "shop/static/img/uploads/items/"
