const num_inputs = document.getElementsByClassName("number-input");
const btn_dec = document.getElementsByClassName("btn-dec");
const btn_inc = document.getElementsByClassName("btn-inc");

const remove = (user_cart_id) => {
   const Http = new XMLHttpRequest();
   const url = `${window.location.origin}/cart/${user_cart_id}/remove`;
   Http.open("POST", url);
   Http.send();
   Http.onreadystatechange = (e) => {
      window.location.replace("/cart");
   };
};

const update = (user_cart_id, quantity) => {
   const Http = new XMLHttpRequest();
   const url = `${window.location.origin}/cart/${user_cart_id}/update`;

   Http.open("POST", url);
   Http.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
   Http.send(JSON.stringify({ quantity: quantity }));

   Http.onreadystatechange = (e) => {
      window.location.replace("/cart");
   };
};

let timeout = null;

const stop_update = () => {
   if (timeout !== null) {
      clearTimeout(timeout);
   }
};

const start_update = (user_cart_id) => {
   stop_update();
   const quantity = parseInt(
      document.getElementById(`input-${user_cart_id}`).value
   );
   timeout = setTimeout(() => {
      update(user_cart_id, quantity);
   }, 1000);
};

for (let i = 0; i < num_inputs.length; i++) {
   num_inputs[i].addEventListener("keydown", (e) => {
      const not_nan = !isNaN(e.key);
      const bspace_or_del = e.key === "Backspace" || e.key === "Delete";
      const ctrl_a = e.ctrlKey && e.key === "a";
      const arrows = e.key === "ArrowRight" || e.key === "ArrowLeft";

      if (not_nan || bspace_or_del || ctrl_a || arrows) return;

      e.preventDefault();
   });
   num_inputs[i].addEventListener("keyup", (e) => {
      const is_first_digit_zero = e.target.value[0] === "0";
      if (is_first_digit_zero) {
         e.target.value = "1";
      }

      if (e.target.value !== "") {
         start_update(e.target.getAttribute("id").replace("input-", ""));
      } else {
         stop_update();
      }
   });
   btn_dec[i].addEventListener("mousedown", (e) => {
      const item_id = e.target.getAttribute("id").replace("btn-dec-", "");
      reduce_quantity(item_id);
   });
   btn_inc[i].addEventListener("mousedown", (e) => {
      const item_id = e.target.getAttribute("id").replace("btn-inc-", "");
      increase_quantity(item_id);
   });
}

const reduce_quantity = (user_cart_id) => {
   const el = document.getElementById(`input-${user_cart_id}`);
   if (el.value !== "" && el.value !== "1") {
      el.value = parseInt(el.value) - 1;
      start_update(user_cart_id);
   } else {
      el.value = 1;
   }
};

const increase_quantity = (user_cart_id) => {
   const el = document.getElementById(`input-${user_cart_id}`);
   if (el.value !== "") {
      el.value = parseInt(el.value) + 1;
   } else {
      el.value = 2;
   }
   start_update(user_cart_id);
};
