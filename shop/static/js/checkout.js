const confirm_order = () => {
   const Http = new XMLHttpRequest();
   const url = `${window.location.origin}/checkout/confirm`;
   Http.open("POST", url);
   Http.send();
   Http.onreadystatechange = (e) => {
      window.location.replace("/orders");
   };
};
