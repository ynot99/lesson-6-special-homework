const order = (order_id, path) => {
   const Http = new XMLHttpRequest();
   const url = `${window.location.origin}/admin/order/${order_id}/${path}`;
   Http.open("POST", url);
   Http.send();
   Http.onreadystatechange = (e) => {
      window.location.replace("/admin/orders/pending");
   };
};
