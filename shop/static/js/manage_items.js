const remove = (item_id) => {
   if (confirm("Are you sure that you want to delete this item?")) {
      const Http = new XMLHttpRequest();
      const url = `${window.location.origin}/admin/item/${item_id}/remove`;
      Http.open("POST", url);
      Http.send();
      Http.onreadystatechange = (e) => {
         window.location.replace("/admin/items");
      };
   }
};
