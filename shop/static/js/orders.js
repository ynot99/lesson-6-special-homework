const cancel_order = (order_id) => {
   const Http = new XMLHttpRequest();
   const url = `${window.location.origin}/order/${order_id}/remove`;
   Http.open("POST", url);
   Http.send();
   Http.onreadystatechange = (e) => {
      window.location.replace("/orders");
   };
};
