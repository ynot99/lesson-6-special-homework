from flask.templating import render_template


def page_not_found(e):
    return render_template("errors/404.j2"), 404
