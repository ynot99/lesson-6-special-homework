from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import (
    StringField,
    PasswordField,
    BooleanField,
    SubmitField,
    IntegerField,
    FileField,
)
from wtforms.validators import (
    InputRequired,
    Length,
    ValidationError,
    EqualTo,
    NumberRange,
)

from shop.models import User


class AddToCartItemForm(FlaskForm):
    quantity = IntegerField(
        "Quantity", validators=[InputRequired(), NumberRange(min=1)]
    )
    submit = SubmitField("Add to cart")


class AddItemForm(FlaskForm):
    name = StringField("Name", validators=[Length(max=30), InputRequired()])
    type = StringField("Type", validators=[Length(max=30), InputRequired()])
    price = IntegerField("Price", validators=[InputRequired()])
    shop = StringField("Shop", validators=[Length(max=30), InputRequired()])
    image = FileField(
        "Add image",
        validators=[
            FileAllowed(["jpg", "jpeg"], "Select jpg format"),
        ],
    )
    submit = SubmitField("Add")


class UpdateItemForm(FlaskForm):
    name = StringField("Name", validators=[Length(max=30), InputRequired()])
    type = StringField("Type", validators=[Length(max=30), InputRequired()])
    price = IntegerField("Price", validators=[InputRequired()])
    shop = StringField("Shop", validators=[Length(max=30), InputRequired()])
    submit = SubmitField("Update")


class RegisterForm(FlaskForm):
    username = StringField(
        "Username", validators=[Length(min=4, max=20), InputRequired()]
    )
    password = PasswordField(
        "Password", validators=[Length(min=8), InputRequired()]
    )
    password_confirm = PasswordField(
        "Password Confirm",
        validators=[
            InputRequired(),
            EqualTo("password", message="Passwords don't match"),
        ],
    )
    submit = SubmitField("Register")

    def validate_username(self, username):
        existing_user = User.query.filter_by(username=username.data).first()
        if existing_user:
            raise ValidationError("That username is taken")


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[InputRequired()])
    password = PasswordField("Password", validators=[InputRequired()])
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Login")

    def validate_username(self, username):
        existing_user = User.query.filter_by(username=username.data).first()
        if not existing_user or not existing_user.check_password(
            self.password.data
        ):
            raise ValidationError("Username or password is invalid")

        self.validated_user = existing_user
