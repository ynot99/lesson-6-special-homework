from flask.helpers import url_for
from flask_login import LoginManager
from werkzeug.utils import redirect

from shop.models import User


login_manager = LoginManager()


def unauthorized_callback():
    return redirect(url_for("auth.login"))


def load_user(id) -> int:
    return User.query.get(id)
