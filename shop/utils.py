import os
import uuid
from datetime import datetime

from flask import request, current_app
from flask_login import current_user
from flask.wrappers import Response
from sqlalchemy.sql.sqltypes import DateTime
from werkzeug.datastructures import FileStorage

from shop.db import db
from shop.models import ShopItem, User, UserCart


def upload_image(image: FileStorage) -> str:
    filename = f"{uuid.uuid4().hex}.jpg"

    if not os.path.exists(current_app.config["IMAGE_UPLOAD_PATH"]):
        os.makedirs(current_app.config["IMAGE_UPLOAD_PATH"])

    image.save(
        os.path.join(f'{current_app.config["IMAGE_UPLOAD_PATH"]}', filename)
    )

    return filename


def remove_image(image_name: str) -> None:
    filename = os.path.join(current_app.config["IMAGE_UPLOAD_PATH"], image_name)
    if os.path.isfile(filename):
        os.remove(filename)


def datetime_format(dtime: DateTime) -> DateTime:
    return dtime.strftime("%Y-%m-%d %H:%M:%S")


def group_order_items(order_items: list, for_admin: bool = False) -> dict:
    id_grouped_order_items = {}
    for item in order_items:
        if item.id not in id_grouped_order_items:
            id_grouped_order_items[item.id] = {
                "order_status": item.order_status,
                "created_at": item.created_at,
                "items": [],
            }
            if for_admin:
                id_grouped_order_items[item.id]["username"] = item.username

        id_grouped_order_items[item.id]["items"].append(
            {
                "name": item.name,
                "shop": item.shop,
                "type": item.type,
                "quantity": item.quantity,
                "fixed_price": item.fixed_price,
            }
        )

    return id_grouped_order_items


def log(response: Response) -> None:
    with open("logs.txt", "a") as f:
        host = request.host
        method = request.method
        path = request.path
        code = response.status_code
        dtime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        f.write(f'{host} - - [{dtime}] "{method} {path}" {code} -\n')

    return response


def get_cart_items_and_total() -> tuple[list, int]:
    cart_items = (
        db.session.query(
            UserCart.id,
            UserCart.shop_item,
            ShopItem.type,
            ShopItem.name,
            ShopItem.shop,
            UserCart.quantity,
            ShopItem.price,
        )
        .join(UserCart, ShopItem.id == UserCart.shop_item)
        .filter(UserCart.user == current_user.id)
        .all()
    )

    total = sum(item.price * item.quantity for item in cart_items)

    return cart_items, total
