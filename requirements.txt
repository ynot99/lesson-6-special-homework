flask==1.1.2
flask_login==0.5.0
flask_migrate==2.7.0
flask_wtf==0.15.1
flask_script==2.0.6
pytest==6.2.4
